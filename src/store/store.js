import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
      //Variable "globales".
      stock: [],
      ventas: [],
      carro: []
    },
    getters: {
      /*Este Getter calcula una nuevo dato a partir
        de las variables establecidas.*/
    },
    mutations: {
      /*Obtener los datos del Stock, desde la DB,
      Mediante una cadena de String que
      se transforma en un JSON.
      */
      setStock(state, data) {
        let jsonData = JSON.parse(data)
        state.stock = jsonData;
      },

      /*Obtener los datos de las Ventas
      Sigue el mismo Procedimiento que Stock
      */
      setVentas(state, data) {
        let jsonData = JSON.parse(data)
        state.ventas = jsonData;
      },

      //Añader un producto al carro para su posterior Venta.
      addCarro(state, item) {
        state.carro.push(item);
      },

      /*Vacia el Carro luego de Concretarse una venta.
      Asi se podra volver a realizar una nueva venta.
      */
      vaciarCarro(state) {
        state.carro = []
      }
    },
    actions: {
      async getStockData(context) {
        return await axios.get('/getData').then(
            response => {
                let data = response.data;
                context.commit('setStock', data)
            }
        )
      },
      async getVentas(context) {
        return await axios.get('/getVentas').then(
            response => {
                let data = response.data;
                context.commit('setVentas', data)
            }
        )
      }
    }
  })
  